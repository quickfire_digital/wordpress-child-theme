<?php
/*--AUTOMATICALLY INSTALL PLUGINS WITH THEME INSTALL--*/
require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';
 
add_action( 'tgmpa_register', 'mytheme_require_plugins' );
 
function mytheme_require_plugins() {
 
     $plugins = array(
    /*--REQUIRED PLUGINS--*/
	    array(
        'name'      => 'Elementor',
        'slug'      => 'elementor',
        'required'  => true,
    ),
		    array(
        'name'      => 'WP Mail SMTP by WPForms',
        'slug'      => 'wp-mail-smtp',
        'required'  => true,
    ),
			    array(
        'name'      => 'Disable Comments',
        'slug'      => 'disable-comments',
        'required'  => true,
    ),
    array(
        'name'      => 'Classic Editor',
        'slug'      => 'classic-editor',
        'required'  => true, 
    ),
    array(
        'name'      => 'Wordfence Security – Firewall & Malware Scan',
        'slug'      => 'wordfence',
        'required'  => true,
    ),
    array(
        'name'      => 'Really Simple SSL',
        'slug'      => 'really-simple-ssl',
        'required'  => true,
    ),
    /*--NOT REQUIRED PLUGINS--*/
    array(
        'name'      => 'Elementor Custom Skin',
        'slug'      => 'ele-custom-skin',
        'required'  => false,
    ),
    array(
        'name'      => 'GDPR Cookie Compliance',
        'slug'      => 'gdpr-cookie-compliance',
        'required'  => false,
    ),
    array(
        'name'      => 'Yoast SEO',
        'slug'      => 'wordpress-seo',
        'required'  => false,
    ),
    array(
        'name'      => 'SVG Support',
        'slug'      => 'svg-support',
        'required'  => false,
    ),
    array(
        'name'      => 'WebP Express',
        'slug'      => 'webp-express',
        'required'  => false,
    ),
    array(
        'name'      => 'Duplicate Post',
        'slug'      => 'duplicate-post',
        'required'  => false,
    ),
    array(
        'name'      => 'Custom Post Type UI',
        'slug'      => 'custom-post-type-ui',
        'required'  => false,
    ),
    array(
        'name'      => 'Advanced Custom Fields',
        'slug'      => 'advanced-custom-fields',
        'required'  => false, 
    )
    
);
    $config = array();
 
    tgmpa( $plugins, $config );
 
}
/*----*/


/*--AUTOMATICLALLY REMOVE HELLO DOLLY AND AKISMET PLUGINS ON THEME INSTALL--*/
function goodbye_dolly() {
    if (file_exists(WP_PLUGIN_DIR.'/hello.php')) {
        require_once(ABSPATH.'wp-admin/includes/plugin.php');
        require_once(ABSPATH.'wp-admin/includes/file.php');
        delete_plugins(array('hello.php'));
    }
}

add_action('admin_init','goodbye_dolly');

function goodbye_akismet() {
    if (file_exists(WP_PLUGIN_DIR.'/akismet/akismet.php')) {
        require_once(ABSPATH.'wp-admin/includes/plugin.php');
        require_once(ABSPATH.'wp-admin/includes/file.php');
        delete_plugins(array('/akismet/akismet.php'));
    }
}

add_action('admin_init','goodbye_akismet');
/*----*/


/*--AUTOMATICALLY ACTIVATE PLUGINS ON INSTALL--*/
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    activate_plugin( 'classic-editor/classic-editor.php' );
    activate_plugin( 'disable-comments/disable-comments.php' );
    activate_plugin( 'elementor/elementor.php' );
    activate_plugin( 'really-simple-ssl/rlrsssl-really-simple-ssl.php' );
/*----*/