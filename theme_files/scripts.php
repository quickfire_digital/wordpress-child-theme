<?php

$assets_path =  get_stylesheet_directory_uri() . "/assets/";

/*--ASSETS & POLYFILL ENQUEUE--*/
wp_enqueue_script( 'script', $assets_path . 'js/script.js', array ( 'jquery' ), time(), true);

// check if site is accessed by a mobile device
if(wp_is_mobile()){
    $mobile = 1;
} else {
    $mobile = 0;
}
$sitedata = array(
    "ismobile" => $mobile,
    "adminajax" => admin_url( 'admin-ajax.php' )
);
// pass the js script the is_mobile variable
wp_localize_script("script", "site_data", $sitedata);
wp_enqueue_style( 'style', $assets_path . 'css/style.css',false,time(),'all');

// POLLYFILL
wp_enqueue_script( 'polyfill', 'https://polyfill.io/v3/polyfill.min.js?features=es2015%2Ces2016%2Ces2017%2Ces2018%2Ces5%2Ces6%2Ces7%2CWebAnimations%2Cdefault', array ( 'jquery' ), '1.1', true);
/*----*/


// GTM ID
$gtm = "";

if($gtm !=""){

    function google_tag_manager_head() {
        ?>
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer',<?php echo $gtm;?>);</script>
            <!-- End Google Tag Manager -->
        <?php
    }
    add_action( 'wp_head', 'google_tag_manager_head' );

    function google_tag_manager_body() {

        ?>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm;?>"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
        <?php

    }

    add_action('after_body_open_tag', 'google_tag_manager_body');
}

function body_begin() {
    do_action('after_body_open_tag');
}
