<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementorChild
 */

/**
 * Load child theme css and optional scripts
 *
 * @return void
 */
/*--ELEMENTOR CHILD THEME--*/
function hello_elementor_child_enqueue_scripts() {
	wp_enqueue_style(
		'hello-elementor-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		[
			'hello-elementor-theme-style',
		],
		'1.0.0'
	);
}
add_action( 'wp_enqueue_scripts', 'hello_elementor_child_enqueue_scripts' );
/*----*/

// LOAD ALL THE PLUGIN CODE
require_once dirname( __FILE__ ) . '/theme_files/plugins.php';
// LOAD ALL THE WORDPRESS SETTINGS
require_once dirname( __FILE__ ) . '/theme_files/wordpress_settings.php';
// LOAD ALL CUSTOM CSS AND JS
require_once dirname( __FILE__ ) . '/theme_files/scripts.php';

// LOAD ALL THE SITE SPECIFIC CODE
if(file_exists(dirname( __FILE__ ) . '/theme_files/site_specific.php')){
	require_once dirname( __FILE__ ) . '/theme_files/site_specific.php';
}
